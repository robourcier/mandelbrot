#include "Mandelbrot.h"


void Mandelbrot::New_Point_mandel(double x, double y, sf::RenderWindow &Window, sf::CircleShape &New, sf::Vector2f C, bool Mandelbrau)
{
	double X_N = x / 2, Y_N = y / 2;
	if (Mandelbrau)
		X_N *= 0.5, Y_N *= 0.5;
	int i = 0;
	
	for (; i < 30 && X_N*X_N + Y_N * Y_N < 4; i++)
	{
		float X_n = X_N;
		X_N = X_N * X_N - Y_N * Y_N+C.x;
		Y_N = 2 * X_n*Y_N+C.y;
	}
	if (X_N*X_N + Y_N * Y_N < 4)
	{
		New.setFillColor(sf::Color::Black);
	}
	else
	{
		if(i>5)
			New.setFillColor(sf::Color(i*5, 255/i,255/i));
		else
			New.setFillColor(sf::Color(i * 5, 0, 0));
	}
	Window.draw(New);
	
}

void Mandelbrot::All_Mandel(sf::RenderWindow &Window, float Precision, bool Mandelbrau, sf::Vector2f C,sf::Vector2f Size_View)
{
	sf::CircleShape New;
	if(Mandelbrau == true)
		for (float i = -((Size_View).x)/2; i < ((Size_View).x) / 2; i += 0.01/Precision)
		{
			for (float j = -((Size_View).y) / 2; j < ((Size_View).y) / 2; j += 0.01/Precision)
			{
				New.setPosition(Width_Window / 2 + i, Height_Window / 2 + j);
				New.setRadius(0.01f/Precision);
				New_Point_mandel(i, j, Window,New,sf::Vector2f(0,0),Mandelbrau);
			}
			Window.display();
		}
	else
		for (float i = -((Size_View).x) / 2; i < ((Size_View).x) / 2; i += 0.01/(Precision*2))
		{
			for (float j= -((Size_View).y) / 2; j < ((Size_View).y) / 2; j += 0.01/(Precision*2))
			{
				New.setPosition(Width_Window / 2 + i, Height_Window / 2 + j);
				New.setRadius(0.01f/(Precision));
				New_Point_mandel(i, j, Window, New, C,Mandelbrau);
			}
			Window.display();
		}
}