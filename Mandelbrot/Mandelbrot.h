#pragma once
#include <SFML/Graphics.hpp>
#define Width_Window 800
#define Height_Window 800
class Mandelbrot
{
public:
	static void New_Point_mandel(double x,double y, sf::RenderWindow &Window, sf::CircleShape &New, sf::Vector2f C,bool Mandelbrau);
	static void All_Mandel(sf::RenderWindow &Window,float Precision,bool Mandelbrau,sf::Vector2f C, sf::Vector2f Size_View);
};

