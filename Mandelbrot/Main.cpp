
#include "Mandelbrot.h"
#include <iostream>
#define Nb_Itteration 50
int main()
{
	sf::Vector2f Complexe;
	Complexe.x = -0.75;
	Complexe.y = +0.1;
	sf::RenderWindow window(sf::VideoMode(Width_Window, Height_Window), "SFML works!");
	sf::View *Vue = new sf::View;
	Vue->setCenter(Width_Window / 2, Height_Window / 2);
	Vue->setSize(5, 5);
	window.clear();
	window.setView(*Vue);
	float Size_Zoom = 1;
	Mandelbrot::All_Mandel(window, Size_Zoom,false,Complexe,Vue->getSize());
	window.display();
	
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Z)
			{
				Size_Zoom *= 1.5;
				
				Vue->setSize(Vue->getSize().x / Size_Zoom, Vue->getSize().y / Size_Zoom);
				window.setView(*Vue);
				window.clear(sf::Color::Black);
				window.display();
				Mandelbrot::All_Mandel(window, Size_Zoom, false, Complexe,Vue->getSize());
				window.display();
			}
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Up)
			{
				Vue->move(0, -10);
				window.setView(*Vue);
				window.display();
			}
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Down)
			{
				Vue->move(0, 10);
				window.setView(*Vue);
				window.display();
			}
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Left)
			{
				Vue->move(-10, 0);
				window.setView(*Vue);
				window.display();
			}
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Right)
			{
				Vue->move(10, 0);
				window.setView(*Vue);
				window.display();
			}
		}
	}
	return 0;
}